---- -*- Mode: Lua; -*-                                                                           
----
---- rpl-backref-test.lua
----
---- © Copyright Jamie A. Jennings, 2019, 2021.
---- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
---- AUTHOR: Jamie A. Jennings

assert(TEST_HOME, "TEST_HOME is not set")
if not rosie_cmd then
   print("\n** Path to rosie executable not set.  Skipping "
	 .. test.current_filename())
   return
end

test.start(test.current_filename())

list = import("list")
util = import "util"
violation = import "violation"
check = test.check

local try = io.open(rosie_cmd, "r")
if try then
   try:close()					    -- found it.  will use it.
else
      error("Cannot find rosie executable")
end
print("Using this rosie executable: " .. rosie_cmd)

testdir = TEST_HOME --ROSIE_HOME .. "/test"

fn = "backref-rpl.rpl"

----------------------------------------------------------------------------------------
test.heading("Running unit-tests on " .. fn)

cmd = rosie_cmd .. " --norcfile test --verbose " .. testdir .. "/" .. fn-- 2>/dev/null"
print()
print(cmd)
results, status, code = util.os_execute_capture(cmd)
if not results then error("Run failed: " .. tostring(status) .. ", " .. tostring(code)); end
print(table.concat(results, '\n'))
if code~=0 then print("Status code was: ", code); end
check(code==0, "Unit tests failed on " .. fn)

----------------------------------------------------------------------------------------
test.heading("Misc backref tests")

check(type(rosie)=="table")
e = rosie.engine.new()
check(e)

ok, _, errs = e:load('foo = backref:bar')
check(not ok)
check(type(errs)=="table")
check(type(errs[1])=="table")
str = violation.tostring(errs[1])
check(str:find("undefined identifier: bar", 1, true))

ok, pkg, _ = e:load('package qux bar = "hi"')
assert(pkg == "qux")

-- RESTRICTION on back references: The target of a back reference must be a
-- local pattern, not a package-qualified (dotted) name.
ok, _, errs = e:load('foo = backref:qux.bar')
check(not ok)
str = violation.tostring(errs[1])
check(str:find("argument does not name a pattern in the current package: qux.bar", 1, true))

ok, _, errs = e:load('localbar = qux.bar')
check(ok)
ok, _, errs = e:load('foo = backref:localbar')
check(ok)

-- RESTRICTION on back references: They match by capture name, so this
-- expression (below) will fail because qux.bar is captured under the name
-- 'qux.bar', but foo is defined to match the capture named 'localbar'.
p, errs = e:compile('qux.bar foo')
assert(p)
m, leftover = p:match('hi hi')
check(not m)

-- Below is the correct way to use back references.
p, errs = e:compile('localbar foo')
assert(p)
m, leftover = p:match('hi hi')
check(m); check(leftover==0)

------------------------------------------------------------------

ok, pkg, _ = e:load('local bar = "hi"')
assert(ok)

ok, _, errs = e:load('foo = backref:bar')
check(ok)

------------------------------------------------------------------

ok, _, errs = e:load('foo = backref:foo')
check(not ok)
check(type(errs)=="table")
check(type(errs[1])=="table")
str = violation.tostring(errs[1])
check(str:find("identifier being defined cannot be referenced on right hand side", 1, true))

-- The . pattern does not capture anything, so any backref to . will never match anything.
ok, _, errs = e:load('foo = backref:.')
check(ok)

ok, _, errs = e:load('foo_plus = foo+')
check(ok)
p = e:compile('foo_plus')
check(p)

m, leftover = p:match('a')
check(not m)
check(leftover == 1)

p = e:compile('{. foo_plus}')
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('ab')
check(not m)
m, leftover = p:match('aa')
check(not m)

-- Define 'any' to capture what . matches (a single char).
ok, _, errs = e:load('any = .; foo = backref:any; foo_plus = foo+; foo_star = foo*')
check(ok)

p = e:compile('foo_plus')
check(p)
m, leftover = p:match('a')
check(not m)

p = e:compile('{any foo_star}')
check(p)
m, leftover = p:match('a')
check(m and (leftover==0))
m, leftover = p:match('aa')
check(m and (leftover==0))
m, leftover = p:match('aaa')
check(m and (leftover==0))

p = e:compile('{any foo_plus}')
check(p)
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('aa')
check(m and (leftover==0))
m, leftover = p:match('aaa')
check(m and (leftover==0))

p = e:compile('{any foo foo}')
check(p)
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('aa')
check(not m)
m, leftover = p:match('aaa')
check(m and (leftover==0))

p = e:compile('{any foo_plus}')
m, leftover = p:match('a')
check(not m)
m, leftover = p:match('ab')
check(not m)
m, leftover = p:match('aa')
check(m and (leftover == 0))
m, leftover = p:match('aaaaaaaa')
check(m and (leftover == 0))

m, leftover = p:match('aaaaaaaaK')
check(m and (leftover == 1))

p, errs = e:compile('backref:any')
check(p)

p, errs = e:compile('backref:any+')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:{any}+')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:{{any}}')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:{any}')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('backref:(any)')
check(not p)
check(violation.tostring(errs[1]):find('is not the name of a pattern'))

p, errs = e:compile('{backref:any}+')
check(p)

m, leftover = p:match('abc')
check(not m)

p, errs = e:compile('any {backref:any}+')
check(p)

m, leftover = p:match('a a')
check(m and (leftover == 0))
m, leftover = p:match('a b')
check(not m)
m, leftover = p:match('aa')
check(not m)

m, leftover = p:match('a a')
check(m and (leftover == 0))
m, leftover = p:match('a b')
check(not m)
m, leftover = p:match('aa')
check(not m)

p, errs = e:compile('backref:xyz')
check(not p)
check(violation.tostring(errs[1]):find('undefined identifier: xyz', 1, true))

-- Monday, July 26, 2021

ok, _, errs = e:load([[
grammar
   a = "a"
in
   A = a
end

-- test A includes A.a "a"

AA = {A backref:A}

-- test AA rejects "a"
-- test AA includes A.a "aa"
]])

check(ok)
if ok then
   p = e:compile('AA')
   check(p)
   if p then
      m, leftover = p:match("aa")
      check(m); check(leftover == 0)
      check(m.subs[1] and m.subs[1].subs[1] and m.subs[1].subs[1].type=="A.a")
   end
end

ok, _, errs = e:load([[
package foo
grammar
   a = "a"
in
   A = a
end

-- test A includes A.a "a"

AA = {A backref:A}

-- test AA rejects "a"
-- test AA includes A.a "aa"
]])

if ok then
   p = e:compile('AA')
   check(p)
   if p then
      m, leftover = p:match("aa")
      check(m); check(leftover == 0)
      check(m.subs[1] and m.subs[1].subs[1] and m.subs[1].subs[1].type=="A.a")
   end
   ok, pkg, errs = e:load("package f; import foo; x = foo.AA")
   check(ok); check(pkg=="f")
   p = e:compile('f.x')
   check(p)
   if p then
      m, leftover = p:match("aa")
      check(m); check(leftover == 0)
      check(m.subs[1] and m.subs[1].subs[1] and m.subs[1].subs[1].type=="A.a")
   end
end



return test.finish()
