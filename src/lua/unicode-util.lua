--------------------------------------------------------------------------------
-- Common case-folding code
--------------------------------------------------------------------------------
unicode_util = {}

function unicode_util.get_utf8_chars(str_hex_vals_space_delim)
    local list_of_hex_codes_in_decimal = {}
    for hex_val in str_hex_vals_space_delim:gmatch("[^%s]+") do
        list_of_hex_codes_in_decimal[#list_of_hex_codes_in_decimal + 1] = tonumber(hex_val, 16)
    end

    return utf8.char(table.unpack(list_of_hex_codes_in_decimal))
end

function unicode_util.case_fold(input_string, case_mapper)
    local case_folded_string = ""
    for _, codepoint_decimal in utf8.codes(input_string) do
        local folded_sequence = ""
        code_point_hex = string.format("%04X", codepoint_decimal)
        local counter = 0
        if (case_mapper[code_point_hex] ~= nil) then
            local mapping
            for key, _ in pairs(case_mapper[code_point_hex]) do
                counter = counter + 1
                mapping = key
            end
            assert(counter == 1)
            folded_sequence = unicode_util.get_utf8_chars(mapping)
        else
            folded_sequence = utf8.char(codepoint_decimal)
        end

        case_folded_string = case_folded_string .. folded_sequence
    end

    return case_folded_string
end

return unicode_util
