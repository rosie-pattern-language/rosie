-- -*- Mode: Lua; -*-
--
-- unicode.lua
--
-- © Copyright Jamie A. Jennings 2018, 2024.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

local rosie = require("rosie")
import = rosie.import
local list = import("list")
local violation = import("violation")

local lpeg = assert(rosie.env.lpeg)

local engine = rosie.engine.new()
local ok, pkgname, errs = engine:loadfile("ucd.rpl")
if not ok then
   error(table.concat(list.map(violation.tostring, errs), "\n"))
end

ucd = dofile("ucd.lua")
enumerated = dofile("enumerated.lua")
util = dofile("util.lua")

UNICODE_VERSION = "15.1.0"
UCD_DIR = assert(rosie.env.ROSIE_HOME) .. "/../../../src/unicode/UCD-" .. UNICODE_VERSION .. "/"

ReadmeFile = "ReadMe.txt"

UnicodeDataFile = "UnicodeData.txt"

PropertyFiles = { {"Block", "Blocks.txt"},
		  {"Script", "Scripts.txt"},
		  {"Category", "extracted/DerivedGeneralCategory.txt"},
		  {"Property", "PropList.txt"},
		  {"Property", "extracted/DerivedBinaryProperties.txt"},
		  {"LineBreak", "extracted/DerivedLineBreak.txt"},
		  {"NumericType", "extracted/DerivedNumericType.txt"},
		  {"GraphemeBreak", "auxiliary/GraphemeBreakProperty.txt"},
		  {"SentenceBreak", "auxiliary/SentenceBreakProperty.txt"},
		  {"WordBreak", "auxiliary/WordBreakProperty.txt"},
	       }

function merge(tbl, new_items)
   assert(tbl)
   for name, value in pairs(new_items) do
      if tbl[name] then
	 error("Value collision for " .. name .. ": " .. value)
      else
	 tbl[name] = value
      end
   end
end

character_db = nil

-- Indexed by Unicode property name (e.g. 'NumericType'), value is a table that
-- maps names to compiled rplx patterns.
property_db = {}
-- Identical structure to property_db, but each value is a table that maps names
-- to rpl source code strings
source_property_db = {}
-- Identical structure to property_db, but each value is a table that maps names
-- to ranges of codepoints
ranges_db = {}

function populate_databases()
   for _, entry in ipairs(PropertyFiles) do
      local property_name = entry[1]
      local filename = entry[2]
      local new_source, new_patterns, ranges =
	 enumerated.processPropertyFile(engine, filename, property_name)
      if not property_db[property_name] then
	 property_db[property_name] = new_patterns
	 source_property_db[property_name] = new_source
	 ranges_db[property_name] = ranges
      else
	 -- A property by the same name exists, but we can merge them if the
	 -- value sets for each are unique (else we throw an error)
	 merge(property_db[property_name], new_patterns)
	 merge(source_property_db[property_name], new_source)
	 merge(ranges_db[property_name], ranges)
      end
   end
end

header_top = [[
-- DO NOT EDIT. THIS FILE WAS GENERATED FROM THE Unicode Character Database.
-- About the Unicode Character Database:
--
]]
header_rest = [[
--
-- This file © Copyright the Rosie Project, 2024.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
]]

function construct_header()
   local nl, err = io.lines(UCD_DIR .. "/" .. ReadmeFile)
   if not nl then error(err); end
   local line = nl()
   header = header_top
   while line do
      if line:sub(1,1)=="#" then
	 header = header .. "--    " .. line .. "\n"
      end
      line = nl()
   end
   header = header .. header_rest
end

-- -----------------------------------------------------------------------------
-- ASCII character classes from the Posix locale (also called C locale)
-- -----------------------------------------------------------------------------

ascii_patterns =
   { ["upper"] =  "[\\x41-\\x5a]",
     ["space"] =  "[\\x09-\\x0d] / [\\x20]",
     ["punct"] =  "[\\x21-\\x2f] / [\\x3a-\\x40] / [\\x5b-\\x60] / [\\x7b-\\x7e]",
     ["alnum"] =  "[\\x30-\\x39] / [\\x41-\\x5a] / [\\x61-\\x7a]",
     ["xdigit"] = "[\\x30-\\x39] / [\\x41-\\x46] / [\\x61-\\x66]",
     ["graph"] =  "[\\x21-\\x7e]",
     ["print"] =  "[\\x20-\\x7e]",
     ["alpha"] =  "[\\x41-\\x5a] / [\\x61-\\x7a]",
     ["digit"] =  "[\\x30-\\x39]",
     ["lower"] =  "[\\x61-\\x7a]",
     ["cntrl"] =  "[\\x00-\\x1f] / [\\x7f]"
  }

-- Ensure the patterns compile without errors
function compile_ascii_patterns()
   local engine = rosie.engine.new()
   for name, source in pairs(ascii_patterns) do
      print("Compiling", name, source:sub(1,40).."...")
      local rplx, errs = engine:compile(source)
      if not rplx then
	 error(table.concat(list.map(violation.tostring, errs), "\n"))
      end
   end
end

-- -----------------------------------------------------------------------------
-- To emulate regexes, we need \w, \d, \s
-- -----------------------------------------------------------------------------

-- ASCII-only is the easy case.  Many (maybe most) regex implementations
-- interpret these abbreviations as ASCII-only, matching as follows:
--   \w == the ranges A-Z, a-z, 0-9, and underscore == [A-Za-z0-9_]
--   \d == the range 0-9 == [0-9]
--   \s == space, tab, carriage return, line feed, and form feed == [ \t\r\n\f]
--
-- Some regex implementations simply create their own definitions for these same
-- shorthands, expanding what they match to include some ostensibly equivalent
-- set of Unicode codepoints (characters).  Matching these in actual text, i.e.
-- encoded in UTF-8 or UTF-16, is not trivial.  Consider what Python does, as
-- described here for Python 3.12.4 (https://docs.python.org/3/library/re.html).
--
-- When using the Python 're' package, the regex \w matches:
--   isalnum() as well as the underscore (_)
--
-- isalnum() matches any of these Unicode categories:
--   isalpha(): Lm, Lt, Lu, Ll, Lo
--   isdecimal(): Nd
--   isdigit(): Numeric_Type=Digit or Numeric_Type=Decimal
--   isnumeric(): Numeric_Type=Digit, Numeric_Type=Decimal or Numeric_Type=Numeric
--
-- Notes:
--   - The isalpha() and isdecimal() predicates are defined in terms of Unicode
--     Categories, while isdigit() and isnumeric() are based on NumericType.
--   - Currently there are only 3 NumericType properties: Numeric, Decimal, Digit
--   - The Python isnumeric() predicate dominates the isdigit() one.
--
-- TODO: 
--
--   For each encoding we wish to support, the first step is to collect all of
--   the codepoints for each character set shorthand (e.g. \w) and merge their
--   ranges.  The result should be a minimal set of disjoint ranges.
--
--   Then convert each codepoint in those ranges into the byte sequence that
--   represents it in an encoding we want to support, e.g. UTF-8.  It may be
--   convenient to explicitly represent ranges at this stage.
--
--   Now build a trie, or rather a set of them.  Look at the start byte for all
--   the characters in the shorthand set (e.g. \w).  Each byte value is the root
--   of a trie whose branches end in a recognized character in the shorthand
--   set.
--
--   IMPLEMENTATION: We have code that we could reuse/adapt in
--   'compile-utf8.lua' to convert codepoints, sets of them, and ranges of them
--   into Rosie patterns for the UTF-8 encoding.  Or we could code from scratch.
--
--   The global variable 'ranges_db' has the data we need:
--     for k,v in pairs(ranges_db['Category']) do print(k); end
--     for k,v in pairs(ranges_db['NumericType']) do print(k); table.print(v); end
--
--   OTHER PERFORMANCE CONSIDERATIONS: Are there cases where it is faster to
--   check whether a byte value is NOT a particular value?  E.g. suppose we had
--   the ranges 0..100, 102..150, and 152..200.  It may be cheaper to check if a
--   byte in the input is in 0..200 and then that it is NOT 101 or 151.


-- -----------------------------------------------------------------------------
-- Top level:  read(); test(); write()
-- -----------------------------------------------------------------------------

function read()
   construct_header()
   character_db = ucd.loadUnicodeData(engine, UnicodeDataFile)
   populate_databases()
   compile_ascii_patterns()
   source_property_db.Ascii = ascii_patterns
end

function test()
   if (not character_db) or (not defined_ranges) then
      error("Run read() first.")
   end
   for prop_name, patterns in pairs(property_db) do
      local ranges = ranges_db[prop_name]
      enumerated.test_property(prop_name, patterns, ranges)
      end
   end
end

function write(optional_directory)
   optional_directory = optional_directory or "/tmp"
   for propname, patterns in pairs(source_property_db) do
      local filename = (optional_directory .. "/" .. propname .. ".rpl")
      print("Writing " .. filename)
      local f, err = io.open(filename, 'w')
      if not f then error(err); end
      f:write(header, "\n")
      f:write("package ", propname, "\n\n")
      for patname, patsource in pairs(patterns) do
	 f:write(patname, " = ", patsource, "\n")
      end
      f:close()
   end
end
