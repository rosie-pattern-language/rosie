#!/bin/sh -x
#
# From https://gitlab.com/gitlab-org/gitlab/-/issues/389055#note_2130455825
#
#   Workaround for
#     https://gitlab.com/gitlab-org/gitlab/-/issues/389055 
#     https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29641
#

git submodule sync --recursive
git fetch --recurse-submodules=yes
#if git submodule update --init --recursive --depth 1000 -v
if git submodule update --init --recursive -v
then
    echo "Successfully got submodules"
else
    git submodule deinit -f --all
    if git submodule update --init --recursive --depth 10 -v
    then
	echo "Successfully got submodules on retry"
    else
	echo "Unsuccessful attempts to get submodules -- remove repository to have a clean restart"
	rm -Rf .git ${CI_PROJECT_DIR}
        # At this point a proper git clone of the project should do the job
        # But there is no environment variable for the repository url including the token
        # Therefore just exit and let the retry succeed
	exit 100
    fi
fi
